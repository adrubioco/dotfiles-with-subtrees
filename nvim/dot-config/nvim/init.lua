vim.opt.compatible = false

NVIM_CONFIG_HOME = os.getenv('XDG_CONFIG_HOME') .. "/nvim"

-- Plugins
require('plugs')

-- SETTINGS
require('preferences')
require('autocompletion')
require('keybindings')

-- AUTOMATICALLY DONE
require('automatic')

-- FILETYPE DEPENDENT
require('html')
require('latex')
require('markdown')
