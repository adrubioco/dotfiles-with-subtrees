-- Basic settings
vim.o.encoding = 'utf-8'
vim.o.number = true
vim.o.syntax = 'enable'
vim.o.clipboard = "unnamedplus"
vim.o.cursorline = true
vim.o.go = 'a'
vim.o.mouse = 'a'
vim.o.hlsearch = true
vim.o.wildmenu = true
vim.o.breakindent = true
vim.o.formatoptions = 'l'
vim.o.splitbelow = true
vim.o.splitright = true
vim.o.hidden = true
vim.o.smartcase = true
vim.o.list = false
vim.o.showbreak= '↪' -- character to show when line is broken


vim.o.background = 'light'
vim.o.termguicolors = true

vim.g.instant_username = 'Adrià'

vim.cmd([[
" Colors {{{
    " Some problematic terminals may need this
    "let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    "let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    autocmd vimenter * ++nested colorscheme solarized8
    let g:airline_solarized_bg='dark'
" }}}
" Caret shape {{{
    if exists('$TMUX')
        let &t_SI = "\ePtmux;\e\e[5 q\e\\"
        let &t_EI = "\ePtmux;\e\e[2 q\e\\"
    else
        let &t_SI = "\e[5 q"
        let &t_EI = "\e[2 q"
    endif
" }}}
" Indenting {{{
    filetype plugin indent on
    set tabstop=4 " show existing tab with 4 spaces width
    set shiftwidth=4 " when indenting with '>', use 4 spaces width
    set expandtab " On pressing tab, insert 4 spaces
    nnoremap + @
" }}}
" Vimscript file settings {{{
    augroup filetype_vim
        autocmd!
        autocmd FileType vim setlocal foldmethod=marker
    augroup END
" }}}
" Folding {{{
    set foldmethod=indent
    set foldlevelstart=20 " Disables automatic closing of all folds on fileopen
    highlight Folded ctermfg=black
    highlight Folded ctermbg=darkred
"}}}
" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" Line numbers and breaks {{{
    set nu
    set wrap
    set linebreak
    set showbreak=>
"}}}
" Interface {{{
    " Enable the list of buffers
    let g:airline#extensions#tabline#enabled = 1
    " Show just the filename
    let g:airline#extensions#tabline#fnamemod = ':t'
"}}}
]])
