vim.g.mapleader = ' '
vim.g.maplocalleader = '\\'

vim.cmd([[
" EasyAlign
    " Start interactive EasyAlign in visual mode (e.g. vipga)
    xmap ga <Plug>(EasyAlign)
    " Start interactive EasyAlign for a motion/text object (e.g. gaip)
    nmap ga <Plug>(EasyAlign)

" NerdTree
    nnoremap <leader>op :NERDTreeToggle<CR>

" Tabs
    nnoremap <leader>tc :tabclose<cr>
    nnoremap <leader>tn :tabedit<cr>
    nnoremap <leader>tl :tabn<cr>
    nnoremap <leader>th :tabp<cr>

" Buffers
    nnoremap <leader>bn :bnext<cr>
    nnoremap <leader>bp :bprevious<cr>
    nnoremap <leader>bs :w<cr>
    nnoremap <leader>bd :bdelete<cr>
    nnoremap <leader>qq :quit<cr>

" Splits
    nnoremap <leader>wh <C-w>h
    nnoremap <leader>wj <C-w>j
    nnoremap <leader>wk <C-w>k
    nnoremap <leader>wl <C-w>l
    nnoremap <leader>wn <C-w>n
    nnoremap <leader>wv :vs<cr>
    nnoremap <leader>wc :split<cr>

" File commands
    nnoremap <leader>fs :w<cr>
    nnoremap <leader>fp :e $MYVIMRC<cr>

" Filetype dependent behavior
    "nnoremap <leader>fc :w! \| !compiler <c-r>%<CR>
    "nnoremap <leader>fp :!opout <c-r>%<CR><CR>
    "nnoremap <leader>fl :!linter <c-r>%<CR>
    "nnoremap <leader>fe :!st tcc -run %<CR>
]])
