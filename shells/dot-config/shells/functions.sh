#!/bin/sh

pdfmerge() {
    output="$1"
    shift
    gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/default -dNOPAUSE -dQUIET -dBATCH -dDetectDuplicateImages -dCompressFonts=true -r150 -sOutputFile="$output" "$@"
}

mkdircd() {
    mkdir -p "$1" || exit
    cd "$1" || exit
}

zat() {
    cd ~/Documents || exit
    file="$(fzf)"
    wait $!
    zathura "$file" & exit
}

open() {
    file="$(fzf)"
    wait $!
    xdg-open "$file" & exit
}

# Change working dir in shell to last dir in lf on exit (adapted from ranger).
#
# You need to either copy the content of this file to your shell rc file
# (e.g. ~/.bashrc) or source this file directly:
#
#     LFCD="/path/to/lfcd.sh"
#     if [ -f "$LFCD" ]; then
#         source "$LFCD"
#     fi
#
# You may also like to assign a key to this command:
#
#     bind '"\C-o":"lfcd\C-m"'  # bash
#     bindkey -s '^o' 'lfcd\n'  # zsh
lf () {
    # Quit shell if nested instead
    [ "$LF_LEVEL" -eq 1 ] && exit
    tmp="$(mktemp)"
    $(which lf) -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        if [ -d "$dir" ]; then
            if [ "$dir" != "$(pwd)" ]; then
                cd "$dir" || exit
            fi
        fi
    fi
}

jump() {
   cd "$(find . -maxdepth 1 -type d | fzf --height 40% --reverse --header='Jump to location')"
}

sigtermprocess() {
    kill -15 "$(pidof "$1")"
}

sigkillprocess() {
    kill -9 "$(pidof "$1")"
}

morg() { # $1 man page
    man -T html "$1" | pandoc --wrap none -r html -w org -L "$XDG_CONFIG_HOME/.config/lua-filters/remove-header-attr.lua" > sudoers.org
}
