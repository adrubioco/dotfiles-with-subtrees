;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; TODO: Theme switch keybinding
;; TODO: Setup global wrap mode

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Adrià Rubio Coll"
      user-mail-address "engolianth@protonmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec
;;                  :family "monospace"
;;                  :size 12
;;                  :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec
;;                                 :family "sans"
;;                                 :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-solarized-light)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Documents/notebooks")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; ORG MODE CONFIG
;;Org mode bibliography
(setq reftex-default-bibliography '("~/Bibliography/references.bib"))

;; see org-ref for use of these variables
(setq org-ref-bibliography-notes "~/Bibliography/notes.org"
      org-ref-default-bibliography '("~/Bibliography/references.bib")
      org-ref-pdf-directory "~/Bibliography/bibtex-pdfs/")

(setq bibtex-completion-bibliography "~/Bibliography/references.bib"
      bibtex-completion-library-path "~/Bibliography/bibtex-pdfs"
      bibtex-completion-notes-path "~/Bibliography/helm-bibtex-notes")

(setq-default prettify-symbols-alist '(("#+BEGIN_SRC" . "†") ("#+END_SRC" . "†")
                                       ("#+begin_src" . "†") ("#+end_src" . "†")
                                       ("#+BEGIN_EXAMPLE" . "⌄⌄⌄") ("#+END_EXAMPLE" . "⌃⌃⌃")
                                       ("#+begin_example" . "⌄⌄⌄") ("#+end_example" . "⌃⌃⌃")
                                       (">=" . "≥")
                                       ("<=" . "≤")
                                       ("=>" . "⇨")))
(setq prettify-symbols-unprettify-at-point 'right-edge)
(add-hook 'org-mode-hook '+org-pretty-mode)
(add-hook 'org-mode-hook 'prettify-symbols-mode)
;; (map-put! org-mode-map
;;           :localleader
;;           () what)

;; (setq prettify-symbols-unprettify-at-point 'right-edge)
;; (add-hook 'org-mode-hook 'prettify-symbols-mode)

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master t)

(setq +latex-viewers '(zathura))


(setq python-shell-interpreter "ipython"
      python-shell-interpreter-args "-i")

;; (add-hook 'java-mode-hook (lambda ()
;;                             (setq c-basic-offset 4
;;                                   tab-width 4
;;                                   indent-tabs-mode NIL)))

(add-hook 'web-mode-hook (lambda ()
                           (setq web-mode-markup-indent-offset 2)
                           (setq web-mode-css-indent-offset 2)
                           (setq web-mode-code-indent-offset 2)))

;; (add-hook 'kotlin-mode-hook (lambda () (setq kotlin-tab-width 2)))

(setq evil-vsplit-window-right t
      evil-split-window-below t)

(defadvice! prompt-for-buffer (&rest _)
  :after '(evil-window-split evil-window-vsplit)
  (+ivy/switch-buffer))

(setq +ivy-buffer-preview t)
(setq which-key-idle-delay 0)
(setq-default major-mode 'org-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                                        ;             LaTeX export             ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (setq org-latex-compiler "lualatex")

;; (setq org-latex-to-pdf-process (list "latexmk -f -pdf %f"))

;; (setq org-latex-pdf-process
;;       (list (concat "latexmk -"
;;                     org-latex-compiler
;;                     " -recorder -synctex=1 -bibtex-cond %b")))

;; (setq org-latex-listings t)

;; (setq org-latex-default-packages-alist
;;       '(("" "graphicx" t)
;;         ("" "grffile" t)
;;         ("" "longtable" nil)
;;         ("" "wrapfig" nil)
;;         ("" "rotating" nil)
;;         ("normalem" "ulem" t)
;;         ("" "amsmath" t)
;;         ("" "textcomp" t)
;;         ("" "amssymb" t)
;;         ("" "capt-of" nil)
;;         ("" "hyperref" nil)))

;; ETBembo RomanOSF
;; (setq org-latex-classes
;;       '(("article"
;;          "\\RequirePackage{fix-cm}
;; \\PassOptionsToPackage{svgnames}{xcolor}
;; \\documentclass[11pt]{article}
;; \\usepackage{fontspec}
;; \\usepackage{sectsty}
;; \\usepackage{enumitem}
;; \\setlist[description]{style=unboxed}
;; \\usepackage{listings}
;; \\lstset{frame=single,aboveskip=1em,
;;         framesep=.5em,backgroundcolor=\\color{AliceBlue},
;;         rulecolor=\\color{LightSteelBlue},framerule=1pt}
;; \\usepackage{xcolor}
;; \\newcommand\\basicdefault[1]{\\scriptsize\\color{Black}\\ttfamily#1}
;; \\lstset{basicstyle=\\basicdefault{\\spaceskip1em}}
;; \\lstset{literate=
;;             {§}{{\\S}}1
;;             {©}{{\\raisebox{.125ex}{\\copyright}\\enspace}}1
;;             {«}{{\\guillemotleft}}1
;;             {»}{{\\guillemotright}}1
;;             {Á}{{\\'A}}1
;;             {Ä}{{\\\"A}}1
;;             {É}{{\\'E}}1
;;             {Í}{{\\'I}}1
;;             {Ó}{{\\'O}}1
;;             {Ö}{{\\\"O}}1
;;             {Ú}{{\\'U}}1
;;             {Ü}{{\\\"U}}1
;;             {ß}{{\\ss}}2
;;             {à}{{\\`a}}1
;;             {á}{{\\'a}}1
;;             {ä}{{\\\"a}}1
;;             {é}{{\\'e}}1
;;             {í}{{\\'i}}1
;;             {ó}{{\\'o}}1
;;             {ö}{{\\\"o}}1
;;             {ú}{{\\'u}}1
;;             {ü}{{\\\"u}}1
;;             {¹}{{\\textsuperscript1}}1
;;             {²}{{\\textsuperscript2}}1
;;             {³}{{\\textsuperscript3}}1
;;             {ı}{{\\i}}1
;;             {—}{{---}}1
;;             {’}{{'}}1
;;             {…}{{\\dots}}1
;;             {⮠}{{$\\hookleftarrow$}}1
;;             {␣}{{\\textvisiblespace}}1,
;;             keywordstyle=\\color{DarkGreen}\\bfseries,
;;             identifierstyle=\\color{DarkRed},
;;             commentstyle=\\color{Gray}\\upshape,
;;             stringstyle=\\color{DarkBlue}\\upshape,
;;             emphstyle=\\color{Chocolate}\\upshape,
;;             showstringspaces=false,
;;             columns=fullflexible,
;;             keepspaces=true}
;; \\usepackage[a4paper,margin=1in,left=1.5in]{geometry}
;; \\usepackage{parskip}
;; \\makeatletter
;; \\renewcommand{\\maketitle}{%
;;   \\begingroup\\parindent0pt
;;   \\sffamily
;;   \\Huge{\\bfseries\\@title}\\par\\bigskip
;;   \\LARGE{\\bfseries\\@author}\\par\\medskip
;;   \\normalsize\\@date\\par\\bigskip
;;   \\endgroup\\@afterindentfalse\\@afterheading}
;; \\makeatother
;; [DEFAULT-PACKAGES]
;; \\hypersetup{linkcolor=Blue,urlcolor=DarkBlue,
;;   citecolor=DarkRed,colorlinks=true}
;; \\AtBeginDocument{\\renewcommand{\\UrlFont}{\\ttfamily}}
;; [PACKAGES]
;; [EXTRA]"
;;          ("\\section{%s}" . "\\section*{%s}")
;;          ("\\subsection{%s}" . "\\subsection*{%s}")
;;          ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
;;          ("\\paragraph{%s}" . "\\paragraph*{%s}")
;;          ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))

;;         ("report" "\\documentclass[11pt]{report}"
;;          ("\\part{%s}" . "\\part*{%s}")
;;          ("\\chapter{%s}" . "\\chapter*{%s}")
;;          ("\\section{%s}" . "\\section*{%s}")
;;          ("\\subsection{%s}" . "\\subsection*{%s}")
;;          ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))

;;         ("book" "\\documentclass[11pt]{book}"
;;          ("\\part{%s}" . "\\part*{%s}")
;;          ("\\chapter{%s}" . "\\chapter*{%s}")
;;          ("\\section{%s}" . "\\section*{%s}")
;;          ("\\subsection{%s}" . "\\subsection*{%s}")
;;          ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))
